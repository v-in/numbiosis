# Table of contents
- [Table of contents](#table-of-contents)
    - [Introdução](#introdução)
    - [Ambiente de desenvolvimento](#ambiente-de-desenvolvimento)
            - [Importantes](#importantes)
            - [Qualidade de vida](#qualidade-de-vida)
    - [Iniciando a aplicação](#iniciando-a-aplicação)
    - [Exportando como site estatico](#exportando-como-site-estatico)
    - [Contribuindo](#contribuindo)
        - [Branchs](#branchs)
        - [Issues](#issues)
        - [Markdowns](#markdowns)
        - [Jupyter Notebooks](#jupyter-notebooks)
        - [Codigo de conduta](#codigo-de-conduta)
        - [Adicionando rotas](#adicionando-rotas)
        - [Adicionando gráficos](#adicionando-graficos)
        - [Componentes de Layout](#componentes-de-layout)
        - [Adicionando métodos](#adicionando-métodos)
        - [Rotas na barra lateral](#rotas-na-barra-lateral)
        - [ReactMarkdown](#reactmarkdown)
    - [Referencias](#referencias)
        - [Markdown cheat-sheet](#markdown-cheat-sheet)
        - [Codenomes](#codenomes)

## Introdução
É de suma importancia a leitura da seção [Contribuindo](#contribuindo) antes de contribuir com o projeto.

## Ambiente de desenvolvimento
O projeto possui alguns arquivos de configuração que visam garantir experiencia de desenvolvimento suave e produtiva. A utilização dos mesmos, no entanto, depende da instalação de alguns plugins. 

Para contribuição com Javascript é recomendada a utilização do editor [VSCode](https://code.visualstudio.com/download) (ou equivalente) com os seguintes plugins:

#### Importantes
* ESLint
> Highlight de erros comuns e ajuste automatico
* Prettier
> Formatação automatica com guia de estilos ja configurado para o projeto
#### Qualidade de vida
* Auto Close Tag
> Fechamento automatico de tags
* Auto Rename Tag
> Rename automatico de ambas extremidades de uma tag

## Iniciando a aplicação

1. Instale seu gerenciador de pacotes javascript favorito
> Recomendado: [yarn](https://yarnpkg.com/lang/en/docs/install/#debian-stable)

2. Clone este repositorio
```console
$ git clone https://gitlab.com/V-in/numbiosis.git
$ cd numbiosis
```
3. Instale dependencias
```console
$ sudo yarn install
```

4. Inicie a aplicação
    1. Modo desenvolvimento com hot code reloading (mudanças ao vivo)
    ```console
    $ sudo yarn dev
    ```
    2. Modo produção (arquivos compilados mimificados e uglyficados) 
    ```console
    $ sudo yarn build
    $ sudo yarn start
    ```
## Exportando como site estatico
É também possivel exportar o projeto como um site estático, que pode ser então hospedado no seu serviço favorito
```console
$ sudo yarn build
$ sudo yarn export #Arquivos são extraidos para a pasta ./out
```

## Contribuindo
### Branchs
- A branch `devel` é aquela na qual mudanças experimentais devem ser publicadas, como adição de rotas e algoritmos. Todos desenvolvedores tem permissão de push para essa branch.
- Cada grupo está livre para criar branchs individuais, nesse caso o nome da branch deve seguir o padrão `<codenome do grupo>/<nome da branch>` eg: `astego/lucas`
- A gerência de branchs de cada grupo (caso criadas) é responsabilidade de seus respectivos lideres
### Issues
- Foi criada uma `Issue` para cada atividade restante no projeto, cada uma dessas estão marcadas com label contendo o codenome do respectivo grupo responsavel.
- A lista de `Issues` pode ser acessada na lateral esquerda da dashboard do Gitlab.
- Ao iniciar resolução de uma atividade, arraste ela para a board `Doing`
- Ao finalizar uma atividade, clique na issue relacionada e crie um `Merge request`, com alvo sendo a branch `devel` ou `master`
### Markdowns
- Markdowns de descrição de cada método devem ser salvos na pasta `lib/markdowns`
- Demonstração de markdown disponivel em `lib/markdowns/demo`
- Utilize do componente [ReactMarkdown](#reactmarkdown) para renderização de Markdown na pagina
### Jupyter Notebooks
- Todo arquivo `.ipynb` deve está localizado na pasta `lib/notebooks/`
- Todo metodo implementado num notebook deve possuir uma explicação didatica em Markdown
### Codigo de conduta
- Por motivos de segurança, nenhuma modificação direta da branch `master` é permitida por desenvolvedor sem classificação de `mantainer`

    > Isso ja foi pre-configurado, logo essa regra é enforçada

- Propostas de mudança e relato de bugs devem ser feitas através de `issues`
- Mudança do codigo presente na branch `master` deve ser feita através da abertura de um [Merge Request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) e so será adicionada à master após revisão e aprovação de um mantainer
### Adicionando rotas
Cada componente exportado por um arquivo .js presente na pasta `./pages` é automaticamente visivel pelo browser na rota `/nome-do-arquivo` 
#### Exemplo
```javascript
//Arquivo pages/teste.js
import React from 'react'
export default () => (
    <div>
        Hello world!
    </div>
)
```
Caso criado o arquivo acima basta acessar o projeto em execução na rota `/teste` (ex: [localhost:3000/teste](localhost:3000/teste)) para ser apresentado com uma pagina que contem a string Hello world!
### Componentes de Layout
Cada pagina pode ser encapsulada por um Layout, para isso basta exporta-la com o layout em especifico como o componente base
#### Exemplo
```javascript
import MethodsLayout from '../layouts/MethodsLayout'
export default () => (
    <MethodsLayout>
        <div>
            Hello world!
        </div>
    </MethodsLayout>
)
```
O componente acima será apresentado com header e barra lateral, assim como as paginas relacionadas à metodos anteriores

### Adicionando graficos
Para adição de gráficos é recomendada a biblioteca [Plotly](https://plot.ly/javascript/react/) que ja foi previamente instalada e pode ser importada do arquivo `components/PlotlyGraph`.
> O link mencionado possui uma rica lista de tutoriais especificos para a API do Plotly em React

#### Exemplo
O exemplo a seguir está disponivel na rota `/graph-demo`

```javascript
//pages/graph-demo.js
import PlotlyGraph from "../components/PlotlyGraph"
import MethodsLayout from "../layouts/MethodsLayout"

export default () => (
  <MethodsLayout>
    <PlotlyGraph
      data={[
        {
          x: [1, 2, 3],
          y: [2, 6, 3],
          type: "scatter",
          mode: "lines+points",
          marker: { color: "red" }
        },
        { type: "bar", x: [1, 2, 3], y: [2, 5, 3] }
      ]}
      layout={{ width: 600, height: 600, title: "A Fancy Plot" }}
    />
  </MethodsLayout>
)
```


### Adicionando métodos

#### GenericMethod
Cada metodo é renderizado pelo componente `components/GenericMethod`, que tem a seguinte API:
``` javascript
//Principais props aceitas pelo GenericMethod
GenericMethod.propTypes = {
  fields: PropTypes.arrayOf(
    PropTypes.shape({
      //Nome interno do campo
      name: PropTypes.string,
      //Placeholder
      placeholder: PropTypes.string,
      //Label acima da entrada
      label: PropTypes.string
    })
  ),
  //Algoritimo a ser chamado
  f: PropTypes.func,
  //Funcao que renderiza o resultado do algoritmo
  renderResult: PropTypes.func,
}
```
> Para utilização mais avançada, como transformação/validação de entrada e exibição de formulários personalizados,  verifique as outras props listadas no arquivo deste componente

> As funções puras utilizadas como algoritmos do método genérico devem ser armazenadas na pasta `lib/algorithms`

#### Modelo mental

O `GenericMethod` é um contrato que requer que a implementação de métodos seja feita como uma simples cadeia de transformações que começa com a entrada do usuario e acaba como um elemento React válido:

    html = renderResult( f( mapFormToArgs ( inputs ) ) )


#### Exemplo
```javascript
import MethodsLayout from "../layouts/MethodsLayout"
import GenericMethod from "../components/GenericMethod"

const fields = [
  {
    name: "a",
    placeholder: "0",
    label: "Primeiro valor"
  },
  {
    name: "b",
    placeholder: "1",
    label: "Segundo valor"
  }
]

const renderResults = result => ( <span>{result}</span>)

//Idealmente importado de ../libs/algorithms
const f = formData => ( parseInt(formData.a) + parseInt(formData.b) )

export default () => (
  <MethodsLayout>
    <GenericMethod f={f} renderResult={renderResults} fields={fields} />
  </MethodsLayout>
)
```
> Exemplo disponivel na rota `/demo` . 

O Componente acima renderiza um formulário com dois campos - "Primeiro valor" e "Segundo valor" - e retorna imprime como resultado a soma dos dois argumentos. 

Qualquer componente pode ser retornado pela funcao `RenderResult`, logo não há limite para os tipos de saida que o método possibilida.

### Rotas na barra lateral
A definição em JSON das rotas disponiveis na barra lateral esquerda do `MethodsLayout` está presente sob `lib/constants/menu.js`, basta editar este arquivo para que uma nova rota lateral seja adicionada

### ReactMarkdown
É possivel a renderização de markdown com um componente React
> Exemplo em `pages/markdown.js`

#### Math
É permitida utilização de LaTex nos arquivos markdown, basta inserir o caractér `$` antes e depois da expressão para renderizar LaTex inline  ou `$$` para renderizar blocos de LaTex. 
> Lembrar que após `$$` é necessaria quebra de linha, bem como ao final da expressão

#### Exemplo
```javascript
import React from 'react'
import ReactMarkdown from '../components/ReactMarkdown'
import MethodsLayout from '../layouts/MethodsLayout'

//Utilize template strings ( delimitado por crases ) para definição de markdown inline
const md = `
# Hello world!
Esse é um markdown renderizado por um componente React

`
/*Tambem é possivel importar markdown de um arquivo .md:
const md = require('../lib/markdowns/demo.md')
*/
export default () => (
    <MethodsLayout>
        <ReactMarkdown source={md}/>
    </MethodsLayout>
)
```

## Referencias
### Markdown cheat-sheet
 https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

### Binder de cálculo numérico
 https://mybinder.org/v2/gh/gcpeixoto/lecture-ipynb/master?filepath=indice.ipynb

### Codenomes
Referência de codenomes - 2018

| Grupo | Codenome |
| ----- | :------: |
| 1     | Neona    |
| 2     | Tarsalia |
| 3     | Archos   |
| 4     | Ptero    |
| 5     | Ydino    |
| 6     | Ornato   |
| 7     | Astego   |
| 8     | Ilianta  |
| 9     | Poda     |
