import MethodsLayout from "../layouts/MethodsLayout"
import GenericMethod from "../components/GenericMethod"

const fields = [
  {
    name: "a",
    placeholder: "0",
    label: "Primeiro valor"
  },
  {
    name: "b",
    placeholder: "1",
    label: "Segundo valor"
  }
]

const renderResults = result => <span>{result}</span>

const f = formData => parseInt(formData.a) + parseInt(formData.b)

export default () => (
  <MethodsLayout>
    <GenericMethod f={f} renderResult={renderResults} fields={fields} />
  </MethodsLayout>
)
