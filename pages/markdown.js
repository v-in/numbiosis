import MethodsLayout from "../layouts/MethodsLayout"
import ReactMarkdown from "../components/ReactMarkdown"

export default () => (
  <MethodsLayout>
    <ReactMarkdown source={require("../lib/markdowns/demo.md")} />
  </MethodsLayout>
)
