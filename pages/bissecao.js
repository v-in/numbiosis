
import ReactMarkdown from "../components/ReactMarkdown"
import MethodsLayout from "../layouts/MethodsLayout"

export default () => (
  <MethodsLayout>
    <ReactMarkdown source={require("../lib/markdowns/bissecao.md")} />
  </MethodsLayout>
)
