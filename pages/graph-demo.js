import PlotlyGraph from "../components/PlotlyGraph"
import MethodsLayout from "../layouts/MethodsLayout"

export default () => (
  <MethodsLayout>
    <PlotlyGraph
      data={[
        {
          x: [1, 2, 3],
          y: [2, 6, 3],
          type: "scatter",
          mode: "lines+points",
          marker: { color: "red" }
        },
        { type: "bar", x: [1, 2, 3], y: [2, 5, 3] }
      ]}
      layout={{ width: 600, height: 600, title: "A Fancy Plot" }}
    />
  </MethodsLayout>
)
