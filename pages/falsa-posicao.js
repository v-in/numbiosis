import GenericMethod from "../components/GenericMethod"
import MethodsLayout from "../layouts/MethodsLayout"
import PlotlyGraph from "../components/PlotlyGraph"
import regula_falsi from "../lib/algorithms/regula_falsi"
import ReactMarkdown from "../components/ReactMarkdown"

export default () => (
  <MethodsLayout>
    <ReactMarkdown source={require("../lib/markdowns/falsa_posicao.md")}/>
    
    <div>
      <h2>Aplicação</h2>
      <div>
        <GenericMethod
          fields={fields}
          f={regula_falsi}
          renderResult={renderResult}
        />
      </div>
    </div>
  </MethodsLayout>
)

const fields = [
  {
    name: "f",
    placeholder: "x**2 - 6",
    label: "Função"
  },
  {
    name: "a",
    placeholder: "0",
    label: "Valor A"
  },
  {
    name: "b",
    placeholder: "4",
    label: "Valor B"
  },
  {
    name: "tol",
    placeholder: "2e-10",
    label: "Tolerância"
  },
  {
    name: "N",
    placeholder: "5",
    label: "Iterações"
  }
]

const renderResult = ({ x_s, fx_s }) => {
  console.log("x_s:", x_s)
  console.log("fx_s:", fx_s)
  return (
    <div>
      <div className="flex-center">
        <PlotlyGraph
          data={[
            {
              x: x_s,
              y: fx_s,
              mode: "markers",
              type: "scatter",
              marker: {
                size: 8,
                color: x_s.map((elem, i) => i)
              }
            }
          ]}
          layout={{
            title: "Resultado",
            width: 600,
            height: 400
          }}
        />
      </div>
      <b>{`Resultado: `}</b>
      <span>{fx_s[fx_s.length - 1]}</span>
    </div>
  )
}
