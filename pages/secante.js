import React from "react"
import MethodsLayout from "../layouts/MethodsLayout"
import GenericMethod from "../components/GenericMethod"
import PlotlyGraph from "../components/PlotlyGraph"
import secant from "../lib/algorithms/secant"
import ReactMarkdown from "../components/ReactMarkdown"
//import universalUrl from "../lib/utils/universalUrl"

export default () => (
  <MethodsLayout>
    <ReactMarkdown source={require("../lib/markdowns/secante.md")} />
    <h2>Aplicação</h2>
    <GenericMethod fields={fields} f={secant} renderResult={renderResult} />
  </MethodsLayout>
)

const renderResult = ({ x_s, fx_s }) => (
  <div className="flex-center">
    <PlotlyGraph
      data={[
        {
          x: x_s,
          y: fx_s,
          mode: "markers",
          type: "scatter",
          marker: {
            size: 8,
            color: x_s.map((elem, i) => i)
          }
        }
      ]}
      layout={{
        title: "Resultado",
        width: 600,
        height: 400
      }}
    />
  </div>
)

const fields = [
  {
    name: "f",
    placeholder: "x**2 - 6",
    label: "Função"
  },
  {
    name: "x0",
    placeholder: "0",
    label: "Primeiro X"
  },
  {
    name: "x1",
    placeholder: "4",
    label: "Segundo X"
  },
  {
    name: "tol",
    placeholder: "2e-10",
    label: "Tolerância"
  },
  {
    name: "N",
    placeholder: "5",
    label: "Iterações"
  }
]
