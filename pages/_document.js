import Document, { Head, Main, NextScript } from "next/document"

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <html>
        <Head>
          <link
            rel="shortcut icon"
            type="image/x-icon"
            href="/static/favicon.ico"
          />
          <link
            rel="shortcut icon"
            type="image/x-icon"
            href="/static/css/katex.min.css"
          />
          <script
            src="/static/js/katex.min.js"
          />
          <script
            src="/static/js/math.min.js"
          />
        </Head>
        <body className="body">
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
