import React from "react"
import MethodsLayout from "../layouts/MethodsLayout"
import ReactMarkdown from "../components/ReactMarkdown"
import newton from "../lib/algorithms/newton-interpolation"

export default () => (
  <MethodsLayout>
    <ReactMarkdown source={require("../lib/markdowns/interpolador_newton.md")} />
    <div>
        <input type="number" id="degree" placeholder="Grau do polinômio" />
        <a onClick={() =>  newton() }>Gerar Polinômio</a>
        <div id="steps"></div>
	</div>
	<br/>
  </MethodsLayout>
)
