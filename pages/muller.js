import GenericMethod from "../components/GenericMethod"
import { chunk } from "lodash"
import muller from "../lib/algorithms/muller"
import MethodsLayout from "../layouts/MethodsLayout"
import ReactMarkdown from "../components/ReactMarkdown"

export default () => (
  <MethodsLayout>
    <ReactMarkdown source={require("../lib/markdowns/muller.md")} />
    <h2>Aplicação</h2>
    <GenericMethod f={muller} renderResult={renderResult} fields={fields} />
  </MethodsLayout>
)

const renderResult = ({ data }) => (
  <table className="table">
    <caption>Resultado</caption>
    <thead>
      <tr>
        <th>Iteracao</th>
        <th>Real</th>
        <th>Imaginario</th>
      </tr>
    </thead>
    <tbody>
      {chunk(data).map((elem, index) => {
        return (
          <tr key={index}>
            <td>{index}</td>
            <td>{elem[0]}</td>
            <td>{elem[1] || 0}</td>
          </tr>
        )
      })}
    </tbody>
  </table>
)

const fields = [
  {
    name: "f",
    placeholder: "x**2 - 6",
    label: "Função"
  },
  {
    name: "p0",
    placeholder: "0",
    label: "Primeiro X"
  },
  {
    name: "p1",
    placeholder: "4",
    label: "Segundo X"
  },
  {
    name: "p2",
    placeholder: "6",
    label: "Terceiro X"
  },
  {
    name: "tol",
    placeholder: "2e-10",
    label: "Tolerância"
  },
  {
    name: "N",
    placeholder: "5",
    label: "Iterações"
  }
]
