import MethodsLayout from "../layouts/MethodsLayout"
import Mathjax from "../components/Mathjax"
import ReactMarkdown from "../components/ReactMarkdown"
import GenericMethod from "../components/GenericMethod"
import pontosFlutuantes from "../lib/algorithms/ponto-flutuante"
import PlotlyGraph from "../components/PlotlyGraph"

export default () => (
  <MethodsLayout>
    <ReactMarkdown source={require("../lib/markdowns/ponto_flutuante.md")} />
    <h2>Aplicação</h2>
    <GenericMethod
      f={pontosFlutuantes}
      renderResult={renderResult}
      fields={fields}
    />
  </MethodsLayout>
)

const renderResult = result => (
  <div>
    <h4>Foram gerados {result.length} valores!</h4>
    <div className="flex-center">
      <PlotlyGraph
        data={[
          {
            x: result,
            y: new Array(result.length)
              .join("0")
              .split("")
              .map(parseFloat),
            mode: "markers"
          }
        ]}
        layout={{
          title: "Resultado",
          width: 600,
          height: 400
        }}
      />
    </div>
  </div>
)

const fields = [
  {
    name: "precisao",
    placeholder: "3",
    label: "Precisão"
  },
  {
    name: "underflow",
    placeholder: "-1",
    label: "Limite Underflow"
  },
  {
    name: "overflow",
    placeholder: "1",
    label: "Limite Overflow"
  }
]
