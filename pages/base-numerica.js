import MethodsLayout from "../layouts/MethodsLayout"
import Mathjax from "../components/Mathjax"
import ReactMarkdown from "../components/ReactMarkdown"
import basenumerica from "../lib/algorithms/base-numerica"
 
export default () => (
  <MethodsLayout>
    <ReactMarkdown source={require("../lib/markdowns/basenumerica.md")} />
    <h2>Aplicação</h2>
 
    <React.Fragment>
      <div className="form">
        <div key={0} className="flex-left units-gap">
          <label className="unit-0 text-right">Decimal (10)</label>
          <div className="unit">
            <input
              key={0}
              type="text"
              onChange={basenumerica.changeDecimal}
              placeholder="Ex: 42"
              id="decimalInput"
            />
          </div>
        </div>
 
        <div key={1} className="flex-left units-gap">
          <label className="unit-0 text-right">Binário (2)</label>
          <div className="unit">
            <input
              key={1}
              type="text"
              onChange={basenumerica.changeBinary}
              placeholder="Ex: 101010"
              id="binaryInput"
            />
          </div>
        </div>
 
        <div key={2} className="flex-left units-gap">
          <label className="unit-0 text-right">Octal (8)</label>
          <div className="unit">
            <input
              key={2}
              type="text"
              onChange={basenumerica.changeOctal}
              placeholder="Ex: 52"
              id="octalInput"
            />
          </div>
        </div>
 
        <div key={3} className="flex-left units-gap">
          <label className="unit-0 text-right">Hexadecimal (16)</label>
          <div className="unit">
            <input
              key={3}
              type="text"
              onChange={basenumerica.changeHexa}
              placeholder="Ex: 2A"
              id="hexaInput"
            />
          </div>
        </div>
      </div>
    </React.Fragment>
 
  </MethodsLayout>
)