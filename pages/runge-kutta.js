import GaussJordan from "../components/GaussJordan"
import ReactMarkdown from "../components/ReactMarkdown"
import MethodsLayout from "../layouts/MethodsLayout"

export default () => (
  <MethodsLayout>
    <ReactMarkdown source={require("../lib/markdowns/runge_kutta.md")} />
  </MethodsLayout>
)
