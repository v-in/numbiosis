import ReactMarkdown from "../components/ReactMarkdown"
import MethodsLayout from "../layouts/MethodsLayout"

export default () => (
    <MethodsLayout>
      <ReactMarkdown source={require("../lib/markdowns/newton_raphson.md")} />
      <h2></h2>
    </MethodsLayout>
)