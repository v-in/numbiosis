import React from 'react'
import GenericMethod from './GenericMethod'
import MatrixInput from './MatrixInput'
import Mathjax from '../components/Mathjax'
import cholesky from '../lib/algorithms/cholesky'
import toLatex from '../lib/utils/toLatex';

class Cholesky extends React.Component {
    state = {
        size: 3,
    }

    _onChange = (event) => {
        event.preventDefault()
        this.setState({
            size: parseInt(event.target.value)
        })
    }

    render() {
        return (
            <div className='top-gap'>
                <form className='form'>
                    <label>Número de equações</label>
                    <input type='number' value={this.state.size} onChange={this._onChange} />
                    <div id="steps"/>
                </form>
                <GenericMethod
                //Eu tenho que alterar aqui
                    renderForm={renderForm(this.state.size)}
                    f={cholesky}
                    mapFormToArgs={mapFormToArgs}
                />
            </div>
        )
    }
}

export default Cholesky


const renderResult = (result) => (
    <div>
        <h5>Passo-a-passo</h5>
        <Mathjax.Provider>
            {
                
            }
        </Mathjax.Provider>
    </div>
)

const mapFormToArgs = (result) => {
    let A = result.map(elem => elem)
    return {
        A
    }
}

const renderForm = (rows) => (onSubmit) => (
    <MatrixInput rows={rows} columns={rows} onSubmit={onSubmit} />
)
