# Método de Newton para Sistemas Não-Lineares

  

## Definição

Este método determina, a cada iteração, a solução da aproximação linear da função, através de uma matriz Jacobiana . A vantagem de utilizá-lo é que, sob certas condições temos que a aproximação inicial, $(​x0​)$, o vetor de função não-lineares, $F$, e a matriz jacobiana, $J$, tem uma sequência que converge para F(x) = 0, com uma taxa quadrática.

  

## Passos

  

Para o método de Newton, basicamente  vamos ir somando um vetor solução $s$ a nossa aproximação inicial, $x$, onde a partir dessa soma teremos um $x$ mais próximo do valor real da função:

$$\sum_{i=0}^{\infty}x_{i+1}=x_i + s_i $$

Obs: Esse processo iterativo apenas vai parar como em métodos anteriores ao atingir um critério de parada, de tal forma que $x^{aproximado} \to x^{real}$


Para encontrarmos esse vetor solução, vamos precisar resolver uma simples equação linear, usando a matriz do jacobiano citada anteriormente:

$$-F(x_i)=J(x_i)S_i $$

Onde $J(x_i)$ é a matriz formada pelas derivadas parciais do vetor de funções $F$. Vamos ver em um exemplo, caso fossemos expandir um caso onde haja duas funções $f_1(x_1,x_2)$ e $f_2(x_1,x_2)$ teriamos o sistema a abaixo:

$$
\begin{bmatrix}

\frac{\partial f_1(x_1,x_2)}{\partial x_1}  & \frac{\partial f_1(x_1,x_2)}{\partial x_2} \\


\frac{\partial f_2(x_1,x_2)}{\partial x_1}  & \frac{\partial f_2(x_1,x_2)}{\partial x_2} \\

\end{bmatrix}

\begin{bmatrix}
s_0 \\

s_1 \\
\end{bmatrix}
=
\begin{bmatrix}
-f_1(x_1,x_2)\\

-f_2(x_1,x_2) \\
\end{bmatrix}
$$