# Método de Newton-Rapshon

## Definição

O método de Newton (ou Método de Newton–Raphson), desenvolvido por Isaac Newton e Joseph Raphson, tem o objetivo de estimar as raízes de uma função. Para isso, escolhe-se uma aproximação inicial para esta. Após isso, calcula-se a equação da reta tangente (por meio da derivada) da função nesse ponto e a interseção dela com o eixo das abcissas, a fim de encontrar uma melhor aproximação para a raiz. Repetindo-se o processo, cria-se um método iterativo para encontrarmos a raiz da função. Em notação matemática, o método de Newton é dado pela seguinte sequência recursiva:

$$
x_{n+1} = x_n - \frac{f(x_n)}{f'(x_n)} , n\in \mathbb{N}
$$

onde $${\textstyle x_{0}}$$ é uma aproximação inicial dada, $${\textstyle n}$$ indica a $${\textstyle n}$$-ésima iteração do algoritmo e $${\textstyle f'(x_{n})}$$ é a derivada da função $${\textstyle f}$$ no ponto $${\textstyle x_{n}.}$$
## Passos

Dados uma aproximação inicial $${\textstyle p_{0}}$$, uma tolerância inicial $${\textstyle tol>0}$$  e o número máximo de iterações $${\textstyle n_{0}}$$, devolve a solução aproximada p ou uma mensagem de erro.

Passo 1: Faça $${\textstyle k = 1}$$

Passo 2: Enquanto $${\textstyle k ≤ n_0  }$$, execute os passos abaixo:
*  Faça $${\textstyle p = p_0 -\frac{f(p_0)}{f'(p_0)}  }$$
* Se $${\textstyle |p - p_0| < tol}$$ ou $${\textstyle |p - p_0| < tol}$$ ou $${\textstyle \frac{|p - p_0|}{|p_0|} < tol}$$, então devolva p como solução e pare.
* Faça $${\textstyle k = k + 1}$$
* Faça $${\textstyle  p_0 = p}$$

Passo 3:  O metodo falhou após $${\textstyle n_0}$$ iterações.

## Interpretação geométrica

Geometricamente, o que o método de Newton faz é o seguinte:
* Dado um ponto $${\textstyle p_{k-1} }$$, calcula a reta tangente a f em $${\textstyle p_{k-1} }$$.
* Encontra o ponto $${\textstyle p*_{k-1} }$$ no qual a reta tangente passa pelo zero.
Toma $${\textstyle p_k = p*_{k-1}}$$.
