# Método de Muller
O método de Muller é uma técnica modificada do Método da Secante, mas que ao contrário dessa, não estima a raiz de uma função prolongando uma reta através de dois pontos(fazendo com que esta reta seja secante a curva da função), e sim utiliza-se de uma parábola através de três pontos para a aproximação da derivada.


Através de um processo iterativo, substitui-se os coeficientes para obter o ponto da parábola que intercepta o eixo abscisso.

## Desenvolvimento do Método
Pelo fato do Método de Muller aproximar uma Parábola, é conveniente descrever uma função de aproximação que tenha a forma quadrática, tal como:

$$f_{i} = a(x - x_{i})^2 + b(x-x_{i}) + c$$

Escolhendo três pontos interceptantes a função: $(x_{0}, f(x_{0}))$ , $(x_{1}, f(x_{1})$ e $(x_{2},f(x_{2}))$ para substituí-los na equação acima, gerando o conjunto de equações:

$$f_{0} = a(x - x_{0})^2 + b(x-x_{0}) + c $$

$$f_{1} = a(x - x_{1})^2 + b(x-x_{1}) + c $$

$$f_{2} = a(x-x_{2})^2 + b(x - x_{2}) + c $$

Para simplificar a função $f$, tomamos $x = 2x_{i} - x_{2}$, o que nos leva a obter o conjunto de equações:

$$f_{0} = a(x_{0}-x_{2})^2 + b(x_{0} - x_{2}) + c $$

$$f_{1} = a(x_{1} - x_{2})^2 + b(x_{1}-x_{2}) + c $$

$$f_{2} = a(x_{2}-x_{2})^2 + b(x_{2} - x_{2}) + c = c $$

A partir destas três equações, podemos determinar as três incógnitas: $a$, $b$ e $c$. Como dado pela última equação, $c = f(x_{2})$, que é o valor da função avaliada na terceira aproximação. Ao substituirmos este coeficiente nas outras equações, obtemos:

$$f(x_{0}) - f(x_{2}) = a(x_{0} - x_{2})^2 + b(x_{0} - x_{2}) $$

$$f(x_{1}) - f(x_{2}) = x(x_{1} - x_{2})^2 + b(x_{1}-x_{2}) $$

Para simplificar esta demonstração e uma possível codificação, definimos as seguintes variáveis:

$$h_{0} = x_{1} - x_{0} $$

$$h_{1} = x_{2} - x_{0} $$

$$d_{0} = {f(x_{1}) - f(x_{0}) \over x_{1} - x_{0}}$$

$$d_{1} = {f(x_{2}) - f(x_{1}) \over x_{2} - x_{1}} $$

Com essas transformações, as equações podem ser escritas de uma forma mais simplificada:

$$b(h_{0}+h_{1}) - a(h_{0}+h_{1})^2 = h_{0}d_{0} + h_{1}d_{1} $$

$$h_{1}b - ah_{1}^2 = h_{1}d_{1} $$

$$c = f(x_{2}) $$

Para encontrarmos a raiz, aplicamos a fórmula quadrática na próxima iteração. Como temos $x_{0}$, $x_{1}$ e $x_{2}$, podemos encontar $x_{3}$ da seguinte maneira:

$$x_{3} - x_{2} = -{2_{c} \over b\pm\sqrt{b^2 - 4ac}} $$

Isolando $x_{3}$,

$$x_{3} = x_{2} - {2_{c} \over b\pm\sqrt{b^2 - 4ac}} $$

Em um programa, os passos acima são repetidos até quando a aproximação estiver com um erro tolerável. Sendo assim, em uma generalização, $x_{0} = x_{i-3}$ , $x_{1} = x_{i-1}$ , $x_{2} = x_{i-1}$ e $x_{3} = x_{i}$ e o erro relativo é calculado como:

$$err_{no} = {x_{3}-x_{2} \over x_{3}} $$

Dos passos acima é possível ver que a equação que estima $x_{3}$ fornece duas raízes. No método de Muller, a estratégia é escolher o sinal de $b$ que majore o denominador, aumentando a velocidade de convergência.

Embora este algoritmo esteja sendo apresentado na busca de raízes de polinômios, ele pode ser utilizado para encontrar raízes de outras classes de funções, da mesma forma que o Método de Newton-Raphson.


