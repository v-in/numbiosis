﻿
# Bissecção

## Definição

É um método numérico iterativo que tem por objetivo encontrar zeros de funções através do cálculo da média aritmética do intervalo em que a função se encontra respeitando uma tolerância que varia de problema para problema.


## Passos

* Primeiramente pegamos o intervalo ao qual se deseja analisar e fazemos a seguinte verificação: 
$$
f(a)  . f(b) < 0
$$

* Caso essa condição seja satisfeita, o caminho estará livre para que o cálculo da raiz seja feito, pois essa inequação nos diz que há, seguramente, ao menos uma raiz real no intervalo especificado.

* Após confirmar que existe uma raiz no intervalo desejado, realizaremos o seguinte cálculo para a obtenção de um $x_k$, onde $k$ diz respeito a iteração atual que está sendo realizada:

$$
x_k = \frac{a+b}{2}
$$

* Esse valor de raiz obtido por $x_k$ deverá obedecer a um tolerância de tal forma que:

$$
\underbrace{|f(x_k)| < E}_{\mathrm{\ E=Tolerância}}
$$
ou
$$
\underbrace{|b-a| < E}_{\mathrm{\ E=Tolerância}}
$$

* Vamos pegar um exemplo simples para que fique melhor compreendido. Considere $f(x) = x² + ln (x)$,
o intervalo $[0,5;1]$ e uma tolerância $|f(x)|<0,05$. Lembre-se que antes de começar os cálculos é 
importante verificar se a inequação $f(a)  . f(b) < 0$ é satisfeita. Para acelerar os passos, 
vamos supor que o problema já lhe afirma isso. Primeiro vamos descobrir os valores de $f(a)$ e $f(b)$:

$$
f(0,5) = - 0,44
$$
$$
f(1) = 1
$$

* Feito isso, podemos notar que o $f(a)$ deu um resultado negativo e o $f(b)$ de
um resultado negativo, guarde essa informação, vamos precisar dela daqui a pouco.

* Agora vamos calcular a média aritmética do intervalo especificado:

$$
x_1 = \frac{0,5+1}{2}
$$

$$
x_1=0,75
$$

* Se pegarmos esse valor e calcularmos o $f(x)$ dele veremos que não atende ao 
que é determinado na tolerância fornecida, logo temos que realizar esse mesmo 
procedimento novamente até que a tolerância seja respeitada.
* Para calcular o próximo $x_k$ deveremos mudar o intervalo procurado, visto que
quando achamos $x_1 = 0,75$ o valor dessa raiz deve estar no próximo intervalo. 
Para saber em que parte do intervalo ela se encontra calculamos o $f(x)$ dela e 
verificamos qual o sinal será obtido na resposta. Se o sinal obtido for positivo
ela ficará no lugar do intervalo anterior que tinha $f(x)$ positivo, caso o sinal
seja negativo ela ficará no lugar do intervalo anterior que tinha $f(x)$ negativo.
No nosso exemplo, o próximo intervalo para o cálculo da média seria:

$$
\underbrace{[0,5; 0,75]}_{Novo\mathrm{\ intervalo}}
$$

* Como podemos notar, o $f(1)$ anteriormente era igual a um valor positivo assim como o $f(0,75)$ também possui um valor positivo, por isso o intervalo $1$ foi trocado pelo intervalo $0,75$. Como ja foi dito, essas iterações se repetem dessa forma até que a tolerância seja satisfeita.
