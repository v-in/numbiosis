﻿
# Sistema de Numeração

### Bases Numéricas

**Sistema  de Numeração Decimal:** Os números decimais são números racionais (Q) não inteiros expressos por vírgulas e que possuem casas decimais, podendo ser positivos ou negativos.
 
Por exemplo:

- {1, 2, 3, 4, 5, 6, ...}
- {-1, -2, -3, -4, -5, -6, ...}

O sistema decimal é o sistema mais utilizado pelos seres humanos, normalmente para indicar quantidades, e é constituído por dez algarismos: 0,1,2,3,4,5,6,7,8,9.

No sistema decimal cada algarismo tem um valor posicional, ou seja, cada algarismo tem um peso de acordo com a sua posição na representação do valor.

$$
d1d2d3_{(10)} = d1*10^2+d2*10^1+d3*10^0
$$

- A posição do algarismo da esquerda para a direita é o valor do expoente que eleva a base.
- O número é obtido através da soma das multiplicações.

Por exemplo:

$$
237_{(10)} = 2*10^2 + 3*10^1 + 7*10^0
$$

**Sistema de Numeração Binário:** É um sistema de numeração posicional em que todas as quantidades se representam com base em dois números. O sistema binário é o sistema mais utilizado por máquinas, uma vez que os sistemas digitais trabalham internamente com dois estados (ligado/desligado, verdadeiro/falso, aberto/fechado). O sistema binário utiliza os símbolos: {0, 1}, sendo cada símbolo designado por bit (binary digit).

Por exemplo:

$$ 
110_{(2)} = 6_{(10)}
$$

**Sistema de Numeração Octal:** O sistema octal é um sistema de numeração de base 8, ou seja, recorre a 8 símbolos (0,1,2,3,4,5,6,7) para a representação de um determinado valor. O sistema octal foi muito utilizado no mundo da computação, como uma alternativa mais compacta do sistema binário, na programação em linguagem de máquina. 

Por exemplo:

$$
71263_{(8)} = 29363_{(10)}
$$

**Sistema de Numeração Hexadecimal:** Sistema de numeração muito utilizado na programação de microprocessadores, especialmente nos equipamentos de estudo e sistemas de desenvolvimento.  Utiliza os símbolos: {0, 1, 2, 3, 4, 5, 6, 7, 8, 9} do sistema decimal e as letras {A, B, C, D, E, F}, equivalentes a A=10, B=11, C=12, D=13, E=14 e F=15.

Por exemplo:

$$
A37E_{(16)} = 41854_{(10)}
$$

### Conversão de Bases Numéricas

#### $$DECIMAL \longleftrightarrow BINÁRIO$$

#### Decimal para Binário

Para a conversão de decimal para binário é muito simples, basta pegar o valor em decimal que queremos converter e realizar uma ou mais **divisões por 2 (base binária)**. Conforme é demonstrado na figura abaixo:

![DecimalBin](../../static/images/decimalbin.jpg)

- O número que queremos converter para a base binária é submetido a múltiplas divisões por 2 (base binária) até que não seja mais possível dividir.
- A sequência do resto de cada divisão mais o resultado da última divisão, do final para o inicio, é o valor decimal representado na base 2 (sistema binário).

#### Binário para Decimal

Para a conversão de binário para decimal basta pegar o valor na base binária que queremos converter e realizar uma ou mais somas da multiplicação dos algarismos (0 ou 1) com a base 2 elevada a n (posição do algarismo).

$$
d1d2d3_{(2)} = d1*2^2+d2*2^1+d3*2^0
$$

- A posição do algarismo da esquerda para a direita é o valor do expoente que eleva a base.
- O número é obtido através da soma das multiplicações.

Por exemplo:

$$
110_{(2)} = 1*2^2 + 1*2^1 + 0*2^0 = 6_{(10)}
$$

Segue o mesmo padrão para a avaliação do valor de um número na base decimal.

#### $ DECIMAL \longleftrightarrow OCTAL $

#### Decimal para Octal

Seguindo a mesma lógica apresentada na conversão de decimal para binário. Para realizar a conversão de decimal para octal basta pegar o valor em decimal que queremos converter e realizar uma ou mais **divisões por 8 (base octal)**. Conforme é demonstrado na figura abaixo:

![DecimalOctal](../../static/images/decimaloctal.png)

- O número que queremos converter para a base binária é submetido a múltiplas divisões por 8 (base octal) até que não seja mais possível dividir.
- A sequência do resto de cada divisão mais o resultado da última divisão, do final para o inicio, é o valor decimal representado na base 8 (sistema octal).

#### Octal para Decimal

Para a conversão de octal para decimal basta pegar o valor na base octal que queremos converter e realizar uma ou mais somas da multiplicação dos algarismos (0, 1, 2, 3, 4, 5, 6, 7) com a base 8 elevada a n (posição do algarismo).

$$
d1d2d3_{(8)} = d1*8^2+d2*8^1+d3*8^0
$$

- A posição do algarismo da esquerda para a direita é o valor do expoente que eleva a base.
- O número é obtido através da soma das multiplicações.

Por exemplo:

$$
71263_{(8)} = 7*8^4 +1*8^3 + 2*8^2 + 6*8^1 + 3*8^0 =  29363_{(10)}
$$

Segue o mesmo padrão para a avaliação do valor de um número na base decimal.

#### $$DECIMAL \longleftrightarrow HEXADECIMAL$$

#### Decimal para Hexadecimal

A conversão de decimal para hexadecimal também é muito simples. Seguindo a mesma lógica apresentada na conversão de decimal para binário, basta pegar o valor em decimal que queremos converter e realizar uma ou mais **divisões por 16 (base hexadecimal)**. Conforme é demonstrado na figura abaixo:

![DecimalHexa](../../static/images/decimalhexa.png)

-  O número que queremos converter para a base binária é submetido a múltiplas divisões por 16 (base hexadecimal) até que não seja mais possível dividir.
- A sequência do resto de cada divisão mais o resultado da última divisão, do final para o inicio, é o valor decimal representado na base 16 (sistema hexadecimal).

#### Hexadecimal para Decimal

Para a conversão de hexadecimal para decimal basta pegar o valor na base hexadecimal que queremos converter e realizar uma ou mais somas da multiplicação dos algarismos (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E, F) com a base 16 elevada a n (posição do algarismo).

$$
d1d2d3_{(16)} = d1*16^2+d2*16^1+d3*16^0
$$

- A posição do algarismo da esquerda para a direita é o valor do expoente que eleva a base.
- O número é obtido através da soma das multiplicações.

Por exemplo:

$$
A37E_{(16)} = 10*16^3 + 3*16^2 + 7*16^1 + 14*16^0 = 41854_{(10)}
$$

Segue o mesmo padrão para a avaliação do valor de um número na base decimal.
