﻿## Interpolador de Newton

O Interpolador de Newton também conhecido como polinômio de Newton é um método para encontrar um polinômio interpolador para um dado conjunto de pontos, onde os coeficientes do polinômio são calculados através de diferenças divididas.

Dado um conjunto de $$k+1$$ pontos:

$$(x_0, y_0), (x_1, y_1), ..., (x_k, y_k)$$

Com todos os pontos $$x$$ distintos, o polinômio de interpolação de um conjunto de pontos na forma de Newton é dado por:

$$P(x) = y_0 + (x-x_0)(DD_{0,1}) + (x-x_0)(x-x_1)(DD_{0,2}) + ... + (x-x_0)(x-x_1) ... (x-x_k)(DD_{0,k})$$

$$DD$$: Diferença dividida.

$$DD_{0,1}$$: Diferença dividida do $$x_0$$ ao $$x_1$$.

### Exemplo:

Utilizando os seguinte pares:

| Par           | (x, y)        |
|:---           | :---          |
| Par 0         | (0, 1)        |
| Par 1         | (1, 0)        |
| Par 2         | (2, 1)        |

Começamos realizando os cálculos para determinar as diferenças divididas:

Ordem 1

$$DD_{0,1} = \frac{y_1 - y_0}{x_1 - x_0} = \frac{0 - 1}{1 - 0} = -1$$$$

$$DD_{1,2} = \frac{y_2 - y_1}{x_2 - x_1} = \frac{1 - 0}{2 - 1} = 1$$

Ordem 2

$$DD_{0,2} = \frac{DD_{1,2} - DD_{0,1}}{x_2 - x_0} = \frac{1 - (-1)}{2 - 0} = 1$$

|| $$x$$           | $$y$$       | $$DD_{k, k+1}$$ |               | $$DD_{k,k+2}$$|
|:---              | :---        | :----           | :---:         | :---:         |
| $$x_0,y_0$$      | 0           | 1               | -1            | 1             |
| $$x_1,y_1$$      | 1           | 0               | 1             |
| $$x_2,y_2$$      | 2           | 1               |

#### Aplicando os resultados:

$$P(x) = y_0 + (x-x_0)(DD_{0,1}) + (x-x_0)(x-x_1)(DD_{0,2})$$

$$P(x) = 1 + (x - 0)(-1) + (x - 0)(x - 1)(1)$$

$$P(x) = 1 + (x)(-1) + (x)(x - 1)$$

$$P(x) = 1 - x + x^2 - x$$

$$P(x) = x^2 - 2x + 1$$

$$$$$

    