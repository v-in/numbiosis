# Método da Secante
Uma grande desvantagem do método de Newton é a necessidade de se obter $f'(x_{k})$ e calcular seu valor numérico a cada iteração.

Uma forma de se contornar este problema é substituir a derivada de $f'(x_{k})$ pelo quociente das diferenças:

$$f'(x_{k}) = { {f(x_{k}) - f(x_{k-1})} \over {x_{k} - x_{k-1}} }$$

onde $x_{k}$ e $x_{k-1}$ são duas aproximações para a raiz.

Neste caso, a função de iteração fica

$$\varphi(x_{k}) = x_{k} - { {f(x_{k})} \over {f(x_{k}) - f(x_{k-1})} }(x_{k} - x_{k-1})$$

ou ainda,

$$\varphi(x_{k}) = x_{k} - { {x_{k-1}f(x_{k}) - x_{k}f(x_{k-1})} \over {f(x_{k}) - f(x_{k-1})} }$$

Observamos que são necessárias duas aproximações para se iniciar o método.

## Interpretação Geométrica
A partir de duas aproximações $x_{k-1}$ e $x_{k}$ , o ponto $x_{k+1}$ é obtido como sendo a abcissa do ponto de intersecção do eixo $ox$ e da reta secante que passa por $(x_{k-1}, f(x_{k-1}))$ e $(x_{k}, f(x_{k}))$:

## Comentários finais

Visto que o Método da Secante é uma aproximação para o método de Newton, as condições para a convergência do método são praticamente as mesmas; acrescenta-se ainda que o método pode convergir se $f(x_{k}) \approx f(x_{k-1})$.
