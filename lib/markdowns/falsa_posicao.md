﻿﻿# Falsa Posição

## Definição

É um método numérico iterativo que tem por objetivo encontrar zeros de funções reais definidas em um intervalo $[a,b]$ partindo da suposição de que haja uma solução de um subintervalo contido em $[a,b]$.


## Passos

* Primeiramente pegamos o intervalo ao qual se deseja analisar e fazemos a seguinte verificação, assim como no método da bisseção: 
$$f(a)  . f(b) < 0$$

* Caso essa condição seja satisfeita, teremos a informação que existe, seguramente, ao menos uma raiz real no intervalo especificado.

* Após confirmar que existe uma raiz no intervalo desejado, realizaremos o seguinte cálculo para a obtenção de um $x_k$, onde $k$ diz respeito a iteração atual que está sendo realizada:

$$
x_k = \frac{a.f(b)-b.f(a)}{f(b)-f(a)}
$$

* Esse valor de raiz obtido por $x_k$ deverá obedecer a um tolerância de tal forma que:
  
$$
\underbrace{|f(x_k)| < E}_{\mathrm{\ E=Tolerância}}
$$

ou

$$
\underbrace{|b-a| < E}_{\mathrm{\ E=Tolerância}}
$$

* Vamos pegar um exemplo simples para que fique melhor compreendido. Considere $f(x) = x² + ln (x)$, o intervalo $[0,5;1]$ e uma tolerância $|f(x)|<0,05$. Lembre-se que antes de começar os cálculos é importante verificar se a inequação $f(a)  . f(b) < 0$ é satisfeita. Para acelerar os passos, vamos supor que o problema já lhe afirma isso. Primeiro vamos descobrir os valores de $f(a)$ e $f(b)$:
$$f(0,5) = - 0,4431$$
$$f(1) = 1$$

* Agora vamos utilizar a fórmula do método para obter um valor para $x_1$ do intervalo especificado:
$$x_1 = \frac{0,5.1 - 1.(-0,4431)}{1-(-0,4431)}$$
$$x_1=0,6535$$
* Se pegarmos esse valor e calcularmos o $f(x)$ dele teremos:
$$f(x_1)=0,0016$$
* Podemos notar que esse valor já é menor que a tolerância pedida, logo esse é o nosso valor de raiz da equação no intervalo especificado para esse problema.
* Caso o valor da raiz não satisfaça a tolerância na primeira tentativa deve-se seguir os mesmos passos feito no [Método da Bisseção](LINK) após a primeira iteração.
