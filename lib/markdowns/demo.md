# This is a header
## This is a subheader
###### Headers all the way down
This is plain text

- This
- Is 
- a
- list

```javascript
//This is a javascript code block
```

Inline math: $ 1 + 1 = 2 $

Block math: 
$$
    2 + 2 = 4
$$


| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

[This is a link](http://www.google.com/search?q=hello+world)

------------

This is an image ![This is an image](../../static/images/mini_logo.png)

[Link to "Headers all the way down"](#headers-all-the-way-down)