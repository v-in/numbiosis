# Eliminação de Gauss

## Definição

Consiste em um algoritmo criado com o intuito de resolver sistemas de equações lineares. É um método que busca manipular o sistema por meio de determinadas operações fundamentais, até chegar em uma matriz triangular, também conhecida como matriz escalonada do sistema.


## Passos

* De forma inicial, é necessário transformar o sistema linear em forma de matriz, dita matriz estendida, usando os coeficientes das variáveis como elementos dessa matriz.
$$
\begin{array}{c}a_{11}x_1+a_{12}x_2+a_{13}x_3 = b_1 \quad(L_1)\\
a_{21}x_1+a_{22}x_2+a_{23}x_3 = b_2 \quad(L_2)\\
a_{31}x_1+a_{32}x_2+a_{33}x_3 = b_3 \quad(L_3)\\
\downarrow\\
\left[
\begin{array}{ccc|c}
	a_{11}&a_{12}&a_{13}&b_{1}\\
	a_{21}&a_{22}&a_{23}&b_{2}\\
	a_{31}&a_{32}&a_{33}&b_{3}
\end{array}
\right]
\end{array}
$$

* É necessário conhecer algumas operações elementares, sendo elas a multiplicação de uma linha da matriz por uma constante não-nula, como também a substituição de uma linha por ela mesma somada a um múltiplo de outra linha e pôr fim a permuta entre linhas.
$$
\begin{array}{c}Multiplicação\ por\ uma\ constante:\\\\
L_2 \leftarrow L_2 .w\ \Rightarrow\ 
\begin{bmatrix}
a_{11}&a_{12}\\
a_{21}&a_{22}\\
\end{bmatrix} \rightarrow
\begin{bmatrix}
a_{11}&a_{12}\\
w.a_{21}&w.a_{22}\\
\end{bmatrix}
\end{array}
$$

$$
\begin{array}{c}Soma\ com\ o\ multiplo\ de\ outra\ linha:\\\\
L_2 \leftarrow L_2 - L_1.w\ \Rightarrow\ 
\begin{bmatrix}
a_{11}&a_{12}\\
a_{21}&a_{22}\\
\end{bmatrix} \rightarrow
\begin{bmatrix}
a_{11}&a_{12}\\
a_{21}-a_{11}.w&a_{22}-a_{12}.w\\
\end{bmatrix}
\end{array}
$$

$$
\begin{array}{c}Permutação\ de\ linhas:\\\\
L_2 \leftarrow L_1\ e\ L_1\leftarrow L_2\ \Rightarrow\ 
\begin{bmatrix}
a_{11}&a_{12}\\
a_{21}&a_{22}\\
\end{bmatrix} \rightarrow
\begin{bmatrix}
a_{21}&a_{22}\\
a_{11}&a_{12}\\
\end{bmatrix}
\end{array}
$$

* Sabendo as operações, as linhas das matrizes serão trabalhadas à fim de obter uma matriz triangular superior, o que significa que todos os elementos a baixo da diagonal principal devem ser nulos.
$$
\left[
\begin{array}{ccc|c}
	a_{11}&a_{12}&a_{13}&b_{1}\\
	a_{21}&a_{22}&a_{23}&b_{2}\\
	a_{31}&a_{32}&a_{33}&b_{3}
\end{array}
\right]\ \rightarrow\ 
\left[
\begin{array}{ccc|c}
	a_{11}^{(k)}&a_{12}^{(k)}&a_{13}^{(k)}&b_{1}^{(k)}\\
	0&a_{22}^{(k)}&a_{23}^{(k)}&b_{2}^{(k)}\\
	0&0&a_{33}^{(k)}&b_{3}^{(k)}
\end{array}
\right]
$$

* Com a forma triangular, ou dita também escalonada, será possível encontrar a solução das equações, pois cada linha da nova matriz possuirá uma incógnita e um valor para ela, usando de substituição, todas as variáveis podem ser encontradas.
$$
\left[
\begin{array}{ccc|c}
	a_{11}^{(k)}&a_{12}^{(k)}&a_{13}^{(k)}&b_{1}^{(k)}\\
	0&a_{22}^{(k)}&a_{23}^{(k)}&b_{2}^{(k)}\\
	0&0&a_{33}^{(k)}&b_{3}^{(k)}
\end{array}
\right]\ \rightarrow\ 
\left\{
\begin{array}{c}
x_3 = \frac{b_3^{(k)}}{a_{33}^{(k)}},\ a_{33}^{(k)} \neq 0\\\\
x_2 = \frac{b_2^{(k)}-a_{23}^{(k)}.x_3}{a_{22}^{(k)}},\ a_{22}^{(k)} \neq 0\\\\
x_1 = \frac{b_1^{(k)}-a_{12}^{(k)}.x_2-a_{13}^{(k)}.x_3}{a_{11}^{(k)}},\ a_{11}^{(k)} \neq 0\\\\
\end{array}
\right. \blacksquare
$$