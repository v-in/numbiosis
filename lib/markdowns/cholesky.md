﻿# Fatoração de Cholesky

## Definição
Trata-se de um algoritmo para resolução de sistemas lineares $Ax=b$ através de uma decomposição da matriz $A$ em uma multiplicação de duas outras, $GG^T$, aonde uma é transposta da outra. Tal método apenas é aplicável em sistemas que gerem uma matriz que seja simétrica e positiva definida.

## Passos

Para a Fatoração de Cholesky ser possível, primeiramente é necessário checar se a matriz associada ao sistema cumpre os requisitos para tal, ou seja, é checado se a matriz é:
- Simétrica: o que ocorre quando sua transposta é igual a ela própria:
$$
A=A^T
$$
- e Positiva Definida: o que é feito averiguando se nela o Critério de Sylvester é satisfeito, ou seja, é necessário verificar se o determinante dos menores principais da matriz, constituídos pelas k primeiras linhas e k primeiras colunas dela, são maiores do que zero:
$$
det(A_k) > 0\text{, onde } k=1,2,...,n \text{ para matrizes }A_{nxn} 
$$

Feito isso, torna-se possível aplicar a fatoração, aonde a matriz $A_{nxn}$ será decomposta em uma triangular inferior $G$ e sua transposta, que é uma triangular superior, $G^T$. Processo esse que é descrito abaixo:

- Para uma matriz $A_{4x4}$ obtém-se uma fatoração da seguinte forma:
 $$
 A = GG^T \Rightarrow
  \begin{bmatrix}
 a_{11} & a_{12} & a_{13} & a_{14} \\
 a_{21} & a_{22} & a_{23} & a_{24} \\
 a_{31} & a_{32} & a_{33} & a_{34} \\
 a_{41} & a_{42} & a_{43} & a_{44}
  \end{bmatrix} =
  \begin{bmatrix}
 g_{11} &             &            &              \\
 g_{21} & g_{22} &            &              \\
 g_{31} & g_{32} & g_{33} &             \\
 g_{41} & g_{42} & g_{43} & g_{44} \\
  \end{bmatrix}
  \begin{bmatrix}
 g_{11} & g_{21} & g_{31} &  g_{41} \\
             & g_{22} & g_{32} & g_{42} \\
             &             & g_{33} & g_{43} \\
            &              &             & g_{44} \\
  \end{bmatrix}

- Tal sistema pode ser resolvido comparando cada coluna de $A$ com a multiplicação de $G$ por cada coluna de $G^T$ o que, em outras palavras, pode ser descrito como $A_{kxn} = GG^T_{kxn} \text{ sendo }k = 1,2,...,n$. Através dessa sequência são obtidos sistemas simples, aonde a cada coluna $k$ calculada os valores de $g_{kn}$ serão obtidos e, após a atualização do $k=k+1$, não será necessários resolver novamente as $k-1$ linhas do próximo sistema gerado.

Com a obtenção dos valores que geram as matrizes triangulares, é possível seguir para a última etapa, a qual consiste de obter efetivamente o vetor $x$ tal que $Ax=b$, sendo isso possível da seguinte forma:
$$
Ax=b \implies (GG^T)x=b \implies G(G^Tx)=b \implies Gy=b
$$
Ou seja, é necessário primeiramente obter o vetor $y$ tal que $Gy=b$ para, apenas depois disso, obter o vetor $x$ através da relação $G^Tx=y$. Fazendo com que, assim, seja obtido o vetor $x$ que resolve o sistema linear proposto no inicio.

