import getConfig from "next/config"
const { publicRuntimeConfig } = getConfig()

/**
 * Returns prefixed url on prod and ci
 * @param {string} url
 */
const universalUrl = url =>
  process.env.CI ? publicRuntimeConfig.CI_PREFIX + url : url

export default universalUrl
